import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { MaterialModule } from './material.module';

import {CdkTableModule} from '@angular/cdk/table';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SidenavResponsiveExample } from './sidenav-responsive-example/sidenav-responsive-example';


@NgModule({
  declarations: [
    SidenavResponsiveExample
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  entryComponents: [SidenavResponsiveExample],
  bootstrap: [SidenavResponsiveExample],
  providers: []
})
export class AppModule { }
